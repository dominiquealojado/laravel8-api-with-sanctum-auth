<?php

namespace App\Http\Controllers;

use App\Models\User;


use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Validator;

class AuthController extends Controller
{
    public function register(Request $request){
    

    	

        $data = $request->validate([
        	 'name' => 'required|string',
    	      'email' => 'required|string|unique:users,email',     
    	      'ip_address' => 'required|ip',     
        ]);

        $array = array("Manila", "Quezon City", "Caloocan City", "Mandaluyong City", "Pasay City", "Pasig City");
    
   
       

    	$user = User::create([
    		'name' => $request->name,
    		'email' => $request->email,
    		'ip_address' => $request->ip_address,
    		'country' => "Philippines",
    		'city' => $array[array_rand($array, 1)],
    		'password' => bcrypt('password'),
    	]);


    	$token = $user->createToken('SanctumToken')->plainTextToken;

    	return response()->json([
    		'status' => true,
    		'data' => $user,
    		'token' => $token,
    		'token_type' => 'Bearer',
    		'message' => 'User Registered Successfully'
    	],201);

 

    }

    public function login(Request $request){
    

        $data = $request->validate([
        	 'password' => 'required|string',
            'email' => 'required|string|email',   
        ]);


    	$user = User::where('email',$request->email)->first();
    	

    	if (!$user || !Hash::check($request->password,$user->password)) {
    		

    		return response()->json([
    			'status' => false,
    			'message' => 'Invalid Credentials'
    		],422);
    	}


    	$token = $user->createToken('SanctumToken')->plainTextToken;

    	return response()->json([
    		'status' => true,
    		'data' => $user,
    		'token' => $token,
    		'token_type' => 'Bearer',
    		'message' => 'User logged in Successfully'
    	],200);
    }

    public function logout(Request $request){

    	auth()->user()->tokens()->delete();

    	return response()->json([
    		'status' => true,
    		'message' => 'User logged out Successfully'
    	],200);



    }
}
