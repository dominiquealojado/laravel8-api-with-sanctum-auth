<?php

namespace App\Http\Controllers;

use App\Models\User;


use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Validator;

class UserController extends Controller
{
    public function index(){
    
        $user = User::all();

        if(!$user){
            return response()->json([
                'status' => true,
                'message' => 'Bad Request'
            ],422);
        }
        
    	return response()->json([
            'status' => true,
            'data' => $user,
            'message' => 'User List'
        ],200);

    }


    public function show(Request $request){

        $user = User::find($request->id);

        if(!$user){
            return response()->json([
                'status' => true,
                'message' => 'Bad Request'
            ],422);
        }

            return response()->json([
                'status' => true,
                'data' => $user,
                'message' => 'User Info'
            ],200);

    }

 
}
