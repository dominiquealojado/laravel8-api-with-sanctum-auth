select u.email,u.name,
(SELECT `meta_value` FROM `tbl_users_meta` where `user_id` = u.id AND `meta_key` = 'ip_address') AS ip_address,
(SELECT `meta_value` from `tbl_users_meta` where `user_id` = u.id AND `meta_key` = 'referrer') AS referrer,
(SELECT `meta_value` from `tbl_users_meta` where `user_id` = u.id AND `meta_key` = 'user_agent') AS user_agent,
img.url_path from `tbl_users_images` as img
LEFT JOIN `tbl_users` as u ON u.id = img.`user_id` ORDER BY u.id ASC;