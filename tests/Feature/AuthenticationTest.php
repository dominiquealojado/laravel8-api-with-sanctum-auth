<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use Laravel\Sanctum\Sanctum;
class AuthenticationTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
   

   public function testRequiredFieldsForRegistration(): void {
     



        $this->json('POST', 'api/register', ['Accept' => 'application/json'])
            ->assertStatus(422)
            ->assertJson([
                "message" => "Validation failed",
                "error" => [
                    "name" => ["The name field is required."],
                    "email" => ["The email field is required."],
                    "ip_address" => ["The ip address field is required."],
                ]
            ]);
    
   }

   public function testSuccessfulRegistration(): void
      {
          $userData = [
              "name" => "John Doe",
              "email" => "doe@example.com",
              "ip_address" => "192.168.1.1",
              'country' => "Philippines",
              'city' => "Manila",
          ];

          $this->json('POST', 'api/register', $userData, ['Accept' => 'application/json'])
              ->assertStatus(201)
              ->assertJsonStructure([
                  "data" => [
                      'id',
                      'name',
                      'email',
                      'ip_address',
                      'country',
                      'city',
                  ]
              ]);
      }


      public function testMustEnterEmailAndPassword(): void
        {
            $this->json('POST', 'api/login')
                ->assertStatus(422)
                ->assertJson([
                    "message" => "Validation failed",
                    "error" => [
                        'email' => ["The email field is required."],
                        'password' => ["The password field is required."],
                    ]
                ]);
        }


        public function testSuccessfulLogin(): void
         {
           Sanctum::actingAs(User::factory()->create());


          $user = User::factory()->create(
            [
                'email' => 'sam@test.com',
                'password' => bcrypt('password'),
            ]
           
          );


             $loginData = ['email' => 'sam@test.com', 'password' => 'password'];

             $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])
                 ->assertStatus(200)
                 ->assertJsonStructure([
                    "data" => [
                        'id',
                        'name',
                        'email',
                    ]
                 ]);

             $this->assertAuthenticated();
         }
}
