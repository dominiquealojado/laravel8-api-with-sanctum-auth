<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register', [AuthController::class, 'register'])->name('register');
Route::post('/login', [AuthController::class, 'login'])->name('login');

     
Route::middleware('auth:sanctum')->group( function () {

		Route::group(['prefix' => "users", 'as' => "user."], function () {
			Route::get('/', [UserController::class, 'index']);
   			Route::get('show/{id?}', [UserController::class, 'show']);
		});

 

    Route::post('/logout', [AuthController::class, 'logout']);
});
